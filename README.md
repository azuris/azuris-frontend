# Description

This project is a third person view 2D android game. 

## The story of the game
The main scope of the game is to collect the Magical Artifacts of Azuris. These artifacts are the key for saving the Azuris's Empire from collapse. These articafts are hidden in different locations in the Azuris's Empire. The task is not that simple that it seems, these artifacts are hidden and well guarded by shadow creatures and dark magicians. This will be a long journey through the occupated regions, all the way to the hearth of Azuris's Empire. As a novice magician you have to overcome all the obstacles in your way in order to reach to Azuris.

## The purpose of the game
The user will gain control over the main character in the game. His goal is to collect all the artifacts by defeating the guarding creatures. Towards to his goal he will gain experience wherewith he will gain more powerful spells by the end of his journey. At the end of the game you have to solve the mistery behind of Azuris's fall in order to save the Empire.

## Game structure
The game will consist of several stages with alternating difficulties. In couple of stages there will be hidden secrets, which may help you to improve your character or may have negative effects on your progress.

## Character
The main character of the game can use offensive and deffensive spells. The player can choose 3 of the learnt spells which he can use.  By leveling up he can learn different types of spells. 

## Spells
The player can use the spell by drawing the specified shape of the spell on the screen. Every spell has his specified strenght and cooldown. The summoning of the spell can be successful or failed, it depends on the accuracy of the shape drawing. No matter of the outcomes wheter is successful or not, the spell will be on cooldown. By increasing the drawing speed the player can increase the damage of the spells.